# Course Master - Django project

You can see the repo at [Django Redis Git project](https://codefirst.iut.uca.fr/git/corentin.lemaire/Django-Redis-project).

## Installation

### Install Python3

Install Python3

`sudo apt-get install python3`

Check installation with python3 --version

Then, install pip3 if not done yet.

`sudo apt-get install python3-pip`

Then, install pipenv with `pip3 install pipenv`

### Django installation

Now, you can install Django by doing `pipenv install django`.

This will create you a virtual environment and install Django into it.

### Django-Redis installation

Type `pip install redis` to install redis.

To run the redis server, type `docker run -d --name redis-stack -p 6379:6379 redis/redis-stack:latest`

## Configuration

In the repo folder, type `pipenv shell`. This will lead you to the venv of the project.

Then, type `python manage.py runserver` when you're in the repo root folder.

## Usage

Now, you can go into http://localhost:8000/redis to access the project.

Once a user is registered to a course, he's automatically subscribed to courses update for all courses he follows into the notifications page.

## Limitations

### Sessions

This app works with sessions. You have to play with this project with 2 browsers to test the pub/sub.

To test pub/sub, please follow these instructions:

Open a browser, register as a Teacher, create a course.

Open another browser, register as a Student, search your course, register to it, and go to notifications.

As the teacher, update, delete or publish a message from the course created.

As a student, you'll see instantly the message.

You can also do this with redis-cli. To do that, in a shell, type:

- `docker start redis-stack`

- `docker exec -it redis-stack bash`

- `redis-cli`

- `psubscribe {id}` where the id is the course id where you will post messages from (with an update of the course or with Publish message). you can retrieve it with `KEYS course:*`

- Update the course or post the message from this course id

OR 

- `docker start redis-stack`

- `docker exec -it redis-stack bash`

- `redis-cli`

- Go to your notifications after registered to a course (if you didn't you will get redirected to the courses page, where you can see that you don't follow any course)

- `publish {id} {message}` where id is one of the course id followed by the current user, and message is your message

**BUT** you can also use only one browser.

While you're a teacher, you're "subscribed" to your own course. So with 2 tabs, it with one, you're on notifications page, and with the other your publish a message, update the course or delete the course, you can also test the pub/sub.

---

### Expiration

Expiration of the course: The course expires after 5 minutes. If a student register to the course, it add 1 minute.

---

### Students management

As a teacher, you can't manage students registered to your course.

---

### Searching

The search looks for title, description and level fo the course. There's no advanced search for the moment.

---

### Pub/sub

Sometimes, I really don't know why, some message published does not appear at all to notifications page. It concerns around 20 % of all messages.

However, you can see my pub/sub system is well implemented by listening to all channels into redis-cli with `PSUBSCRIBE *`. It concerns my Django implementation. Sorry for this problem, but if you see the solution, please tell me.

Finally, one evening, I tested the pub/sub and got a lot of weird errors like Invalid Protocol b'8. The following day, I tested again and couldn't get these errors back. So I assume these errors are "fixed", but if you encounter these errors, like weird errors when pub/sub, try again later, with new accounts, I totally don't understand what's going on when these problems occurs.

## Bugs

If you see any bugs, please tell me. I really tried my best by testing every feature every time at every change, but I can miss some tricky things. So please, feel free to mail me at corentin.lemaire@etu.uca.fr to report any bug!

## Django model

I'm using django model a little in my code only to create me some "barriers". this standardize my objects from redis to avoid lot's of bugs. This is only kinda sort of structure to format my data.

## Pub/Sub

To implement the Redis pub/sub in Django, I decided to use this mechanism:

- The `notifications_view` is firstly displayed

- This view calls the `notifications` function thanks to HTML and JS

- This calls the get_message function, waiting for a new message

- While waiting this message, the HTMl refreshs the page every 10s to avoid the timeout

- When a message is received, `notifications` function calls back `notifications_view` and this repeats.

It stops when the user change page. So, the page is constantly "loading" waiting for a message. This allowed me to get real-time message. It's not the most efficient 
way to do this but I really didn't want to use websockets or JS libraries to do this. It seems the fastest easiest way to do it.

Also, I saw in some docs we could perform some async functions in Django, but seems kinda long and heavy to implement, so I choose only this method.