from django.urls import path
from . import views

# Defining my routes here, to associate my URLs, my routes names and my views functions.
urlpatterns = [
    path('', views.home, name='home'),
    path('register_form', views.register_form, name='register_form'),
    path('register', views.register, name='register'),
    path('login_form', views.login_form, name='login_form'),
    path('login', views.login, name='login'),
    path('courses', views.courses, name='courses'),
    path('change_profile', views.change_profile, name='change_profile'),
    path('profile', views.profile, name='profile'),
    path('logout', views.logout, name='logout'),
    path('create_course', views.create_course_view, name='create_course'),
    path('create_course_form', views.create_course_form, name='create_course_form'),
    path('search', views.search_view, name='search'),
    path('search_form', views.search_form, name='search_form'),
    path('notifications_view', views.notifications_view, name='notifications_view'),
    path('notifications', views.notifications, name='notifications'),
    path('clear_notifications', views.clear_notifications, name='clear_notifications'),
    path('publish_message', views.publish_message, name='publish_message'),
    path('publish_message_form', views.publish_message_form, name='publish_message_form'),
    path('course_register/<str:course_id>', views.course_register_view, name='course_register'),
    path('course_unregister/<str:course_id>', views.course_unregister_view, name='course_unregister'),
    path('update_course/<str:course_id>', views.update_course_view, name='update_course'),
    path('update_course_form/<str:course_id>', views.update_course_form, name='update_course_form'),
    path('delete/<str:course_id>', views.delete_course_view, name='delete_course'),
    path('<str:course_id>', views.details, name='details'),
]