# Create indexes for the search feature
def create_index(redis_connection):
    # Testing if the index is already created not to throw an exception when we launch the app multiple times (and i swear it was useful when I devlopped)
    # Thanks to the great RediSearch module of Redis Stack (I'll repeat the Redis doc) I can implement a search feature on every property indexed (except on students list for confidentiality of course)
    # So here, I'm indexing every member of my model because I want to expose every property to let user search in every property (except on students list for confidentiality of course)
    # I'm using hset/hget... because it's my favorite one + it's a fast storage data type
    redis_connection.execute_command('FT.DROPINDEX', 'idx:courses')
    redis_connection.execute_command('FT.CREATE', 'idx:courses', 'ON', 'HASH', 'PREFIX', '1', 'course:', 
                'SCHEMA', 
                'title', 'TEXT',
                'summary', 'TEXT',
                'level', 'TEXT',
                'places', 'NUMERIC',
                'teacher', 'TEXT', # Id of the teacher, without the teacher:
                )

    # Same for persons
    redis_connection.execute_command('FT.DROPINDEX', 'idx:persons')
    redis_connection.execute_command('FT.CREATE', 'idx:persons', 'ON', 'HASH', 'PREFIX', '1', 'person:', 
                'SCHEMA', 
                'name', 'TEXT',
                'role', 'TEXT'
                )