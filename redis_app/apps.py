from django.apps import AppConfig

# Configuring redis as I saw it on the doc.
class RedisAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'redis_app'
